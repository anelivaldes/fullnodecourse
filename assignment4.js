const express = require('express');
const app = express();

let users = [];

app.set('view engine', 'ejs');
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({extended: true})) // for parsing application/x-www-form-urlencoded

app.post('/add-user', (req, res, next) => {
    if (req.method === 'POST') {
        console.log(req.body);
        users.push(req.body.username);
        res.redirect('/users');
    }
});

app.use('/users', (req, res, next) => {
    res.render('users.ejs', {users: users});
});

app.use('/clear-users', (req, res, next) => {
    users = [];
    res.redirect('/users');
});

app.use('/', (req, res, next) => {
    res.render('home.ejs', {name: 'Aneli'});
});
app.listen(3000);
