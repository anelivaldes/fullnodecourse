const http = require('http');
const server = http.createServer((req, resp) => {
    const url = req.url;
    const method = req.method;
    if (url === '/') {
        resp.setHeader('Content-type', 'text/html');
        resp.write('Hello from Node Assignment 1');
        resp.write('<form method="POST" action="/create-user"><input type="text" name="username"><button type="submit">Create</button></form>')
        resp.end();
    }
    if (url === '/users') {
        resp.setHeader('Content-type', 'text/html');
        resp.write('<ul><li>User1</li><li>User2</li></ul>');
        resp.end();
    }
    if (url === '/user-created') {
        resp.setHeader('Content-type', 'text/html');
        resp.write('<h1>User created</h1><a href="/">Back to home</a><a href="/users">List of users</a>');
        resp.end();
    }
    if (url === '/create-user' && method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
            body.push(chunk);
        })
        req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            const message = parsedBody.split('=')[1];
            console.log(message);
            resp.statusCode = 302;
            resp.setHeader('Location', '/user-created');
            resp.end();
        })
    }

});
const port = 3002;
console.log('Listening on port', port);
server.listen(port);