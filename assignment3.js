const express = require('express');
const path = require('path');
const productRouter = require('./routes/products');

const app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.use('/products', productRouter);

app.use('/pictures',(req,res)=>{
    res.sendFile(path.join(__dirname, 'public','IMG_4919.JPG'));
})

app.use('/home',(req,res)=>{
    res.sendFile(path.join(__dirname, 'views','home.html'));
})

app.use('/',(req, res)=>{
    res.send('Express serving static files');
});

app.listen(3000);