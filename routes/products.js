const express = require('express');

const router = express.Router();

router.use('/add', (req, resp) => {
    resp.send('Page to add a product');
});

router.use('/remove', (req, resp) => {
    resp.send('Page to remove a product');
});

router.use('/', (req, resp) => {
    resp.send([{ id: 1, name: 'shoes' }, { id: 1, name: 'shirt' }]);
});

module.exports = router;